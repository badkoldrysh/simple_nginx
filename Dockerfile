FROM nginx:latest

RUN rm /etc/nginx/conf.d/default.conf

COPY ./src /usr/share/nginx/html
COPY site.conf /etc/nginx/conf.d/default.conf
